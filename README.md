![Exemplo](https://i.imgur.com/UW2JUEN.png)
# Tutorial
Em **Editar Perfil**, localize o campo **Resumo** e cole o texto abaixo respeitando os marcadores:

### VAC

```
[h1]1 banimento VAC em registro | [url=https://support.steampowered.com/kb_article.php?ref=7849-Radz-6869&l=brazilian]Informações[/url]
1 dia(s) desde o último banimento[/h1]
```
### Banimento de Jogo

```
[h1]1 banimento de jogo em registro | [url=https://support.steampowered.com/kb_article.php?ref=6899-IOSK-9514&l=brazilian]Informações[/url]
1 dia(s) desde o último banimento[/h1]
```

### Banimento VAC + Banimento de jogo

```
[h1]1 banimento VAC em registro | [url=https://support.steampowered.com/kb_article.php?ref=7849-Radz-6869&l=brazilian]Informações[/url]
1 banimento de jogo em registro | [url=https://support.steampowered.com/kb_article.php?ref=6899-IOSK-9514&l=brazilian]Informações[/url]
3 dia(s) desde o último banimento[/h1]
```

![Exemplo](https://i.imgur.com/x5Q35Mh.png)
